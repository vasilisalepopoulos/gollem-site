
# Name of all the necessary directories
import os
from openpyxl.styles import Alignment, Border, Side
from openpyxl.styles.borders import BORDER_THIN

BACKORDERS_DIR = 'backorders'
STOCK_DIR = 'stock'
IMAGES_DIR = 'images'
SMALL_IMAGES_DIR = os.path.join('images', 'small')
LARGE_IMAGES_DIR = os.path.join('images', 'large')
CSV_DIR = 'csv files'
OUTPUT_DIR = 'output'
LOG = 'logs'

# A list of all the directories
DIRECTORIES = [BACKORDERS_DIR, STOCK_DIR, IMAGES_DIR, CSV_DIR, OUTPUT_DIR, LOG, SMALL_IMAGES_DIR, LARGE_IMAGES_DIR]

# Column names of the sheet prototype
# In English
COLUMNS_EN = ['SKU', 'URL', 'Image', 'Description', 'Barcode', 'SRP', 'Stock', 'Backorder']

# In Greek
COLUMNS_GR = ['Κωδικός', 'URL', 'Προϊόν', 'Περιγραφή', 'Barcode', 'ΠΤΛ', 'Απόθεμα', 'Αναμένεται']

# Dictionaries
# All the supplier's codenames
WHOLESALE_PRODUCTS = ['UND', 'PAL', 'PYR', 'NEC', 'MEZ',
                      'DM', 'NJ', 'WR', 'NONN', 'NOXT',
                      'CAR', 'MC']

WHOLESALER_CODES = WHOLESALE_PRODUCTS

MINIMUM_STOCK = 10
MINIMUM_BACKORDER = 6

# Type of products that are not needed
NOT_NEEDED = {'UND': '\*|Λούτρινο|Λούτρινα|Κούπα',
              'PAL': '\*',
              'PYR': '\*',
              'NEC': '\*',
              'MEZ': '\*',
              'DM': '\*',
              'NJ': '\*',
              'WR': '\*|Display|Αυτοκίνητο 3D',
              'NONN': '\*|Σπαθί',
              'NOXT': '\*',
              'CAR': '\*',
              'MC': '\*'
              }


COLUMN_WIDTH = 100
ROW_HEIGHT = 130

IMAGE_CSS_SELECTOR = "div.s-result-item:nth-child(1) > div:nth-child(1) > span:nth-child(1) > div:nth-child(1) > " \
                     "div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > " \
                     "span:nth-child(1) > a:nth-child(1) > div:nth-child(1) > img:nth-child(1)"


BARCODE_SEARCH = "https://www.amazon.co.uk/s?k="



BORDER_THIN = Border(
            left=Side(border_style=BORDER_THIN, color='00000000'),
            right=Side(border_style=BORDER_THIN, color='00000000'),
            top=Side(border_style=BORDER_THIN, color='00000000'),
            bottom=Side(border_style=BORDER_THIN, color='00000000')
        )

