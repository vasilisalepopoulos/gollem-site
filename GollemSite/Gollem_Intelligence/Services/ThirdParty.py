import dropbox
import json
import urllib
from urllib import request
import time
from difflib import SequenceMatcher
from urllib.request import Request
from GollemSite.Gollem_Intelligence.Services import api_global_variables
import os
from GollemSite import settings


def is_square(height, width):
    return True if height - width < 200 else False


def has_correct_resolution(height, width):  # Resolution should be greater than 500x500
    return True if height >= 500 and width >= 500 else False


def name_matches(dataframe_text, json_text):
    return True if SequenceMatcher(None, dataframe_text, json_text).ratio() >= 0.8 else False


class GoogleCustomEngine:
    url = None
    value = None

    def __init__(self, value):
        self.value = value

    def get_url_of_image(self):

        self.url = "https://www.googleapis.com/customsearch/v1?" \
                   "key={}&cx={}&searchType=image" \
                   "&q={}".format(api_global_variables.GCE_API_KEY, api_global_variables.GCE_CX, self.value)
        try:
            req = Request(self.url, headers={'User-Agent': 'Mozilla/5.0'})
            response = json.load(urllib.request.urlopen(req))
            if int(response.get("searchInformation").get('totalResults')) != 0:
                for r in response.get('items'):
                    if is_square(r.get('image').get('height'),
                                 r.get('image').get('width')) & \
                            has_correct_resolution(r.get('image').get('height'),
                                                   r.get('image').get('width')):
                        url_link = r.get('link')
                        return url_link
                time.sleep(3)
            else:
                return None
        except urllib.request.HTTPError:
            print('Error while getting url')


class DropboxAPI:
    connection = None
    dropbox_path = None
    destination_path = None
    list_images = []

    def __init__(self):
        self.connection = dropbox.Dropbox(api_global_variables.DROPBOX_ACCESS_TOKEN)

    def both_sizes_exist(self, product_code):
        response = self.connection.files_search(api_global_variables.DROPBOX_IMAGE_FOLDER, product_code)

        if len(response.matches) == 2:
            return True
        else:
            return False

    def find_large_image(self, product_code):
        response = self.connection.files_search(api_global_variables.DROPBOX_LARGE_IMAGE_FOLDER, product_code)
        return response

    def download_large_image(self, product_code):
        dropbox_path = os.path.join(api_global_variables.DROPBOX_LARGE_IMAGE_FOLDER,
                                    product_code,
                                    ".*")
        destination_path = os.path.join(settings.BASE_DIR,
                                        'static', 'Files', 'images', 'large',
                                        product_code)

        self.connection.files_download_to_file(dropbox_path, destination_path
                                               )

    def find_small_image(self, product_code):
        response = self.connection.files_search(api_global_variables.DROPBOX_SMALL_IMAGE_FOLDER, product_code)
        return response

    def download_small_image(self, product_code):
        dropbox_path = self.find_small_image(product_code).matches[0].metadata.path_display

        destination_path = os.path.join(settings.BASE_DIR,
                                        'static/Files/images/small',
                                        product_code + ".jpg")
        self.connection.files_download_to_file(destination_path, dropbox_path)
        return destination_path

    def upload_new_large_image(self, path):
        with open(path, 'rb') as f:
            self.connection.files_upload(f.read(), api_global_variables.DROPBOX_LARGE_IMAGE_FOLDER)

    def upload_new_small_image(self, path):
        with open(path, 'rb') as f:
            self.connection.files_upload(f.read(), api_global_variables.DROPBOX_SMALL_IMAGE_FOLDER)

    def process_entries(self, entries):
        for entry in entries:
            self.list_images.append(entry.name)