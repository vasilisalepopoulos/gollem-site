# pull official base image
FROM ubuntu:latest
# set work directory
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0
ENV DEBIAN_FRONTEND=noninteractive

# install psycopg2
RUN apt-get clean && apt-get update -y \
    && apt-get install --no-install-recommends -y python3.8 \
    && apt-get install --no-install-recommends -y python3-pip \
    && apt-get install --no-install-recommends -y python3-setuptools\
    && apt-get install --no-install-recommends -y redis\
    && apt-get install --no-install-recommends -y vim\
    && apt-get install --no-install-recommends -y mongodb

COPY ./requirements.txt .
RUN pip3 install -r requirements.txt


# copy project
COPY . .

# add and run as non-root user
# RUN adduser myuser
USER root

# run gunicorn
CMD gunicorn GollemSite.wsgi:application --bind 0.0.0.0:$PORT

