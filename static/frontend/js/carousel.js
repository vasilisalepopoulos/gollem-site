headers = ["Scrape!", "Automate!", "Achieve"];
paragraphs = [
  "Search Amazon to find more opportunitites",
  "Newsletters, pricelists, catalogs... you name it",
  "Greatness",
];

function timedText() {
  setTimeout(fist_message, 7000);
  setTimeout(second_message, 14000);
  setTimeout(third_message, 21000);
}

function fist_message() {
  var parent = document.getElementById('message')

  document.getElementById("header").remove();
  document.getElementById("paragraph").remove();

  var header = document.createElement("h1")
  header.setAttribute("id", "header");

  var paragraph = document.createElement("p")
  paragraph.setAttribute("id", "paragraph");

  parent.appendChild(header)
  parent.appendChild(paragraph)

  document.getElementById("header").innerHTML = headers[0];
  document.getElementById("paragraph").innerHTML = paragraphs[0];
}

function second_message() {
  var parent = document.getElementById('message')

  document.getElementById("header").remove();
  document.getElementById("paragraph").remove();

  var header = document.createElement("h1")
  header.setAttribute("id", "header");

  var paragraph = document.createElement("p")
  paragraph.setAttribute("id", "paragraph");

  parent.appendChild(header)
  parent.appendChild(paragraph)

  document.getElementById("header").innerHTML = headers[1];
  document.getElementById("paragraph").innerHTML = paragraphs[1];
}

function third_message() {
  var parent = document.getElementById('message')

  document.getElementById("header").remove();
  document.getElementById("paragraph").remove();

  var header = document.createElement("h1")
  header.setAttribute("id", "header");

  var paragraph = document.createElement("p")
  paragraph.setAttribute("id", "paragraph");

  parent.appendChild(header)
  parent.appendChild(paragraph)

  document.getElementById("header").innerHTML = headers[2];
  document.getElementById("paragraph").innerHTML = paragraphs[2];
}