import mimetypes
import os

import pandas
from django.shortcuts import render
from scheduler import tasks
from wsgiref.util import FileWrapper
from django.http import StreamingHttpResponse
from GollemSite import settings
import logging

# Create your views here.

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.basicConfig(level=logging.INFO)


def main(request):

    return render(request, template_name='main.html')


def option_page(request):
    return render(request, template_name='option-page.html')


def about_me(request):
    return render(request, template_name='')


def upload(request):
    if request.method == "POST":
        print('going to upload stock file(s)')
        stock_file = request.FILES['stock_file']
        df = pandas.read_excel(stock_file)
        tasks.update_stock.delay(df.to_json())
    return render(request, template_name='upload.html')


def download_file(request):
    the_file = open(os.path.join(settings.BASE_DIR, 'static', 'Files', 'output', 'pricelist.xlsx'), "rb")
    filename = os.path.basename("Pricelist.xlsx")
    c_type = mimetypes.guess_type(the_file.name)[0]
    chunk_size = 8192
    response = StreamingHttpResponse(FileWrapper(the_file, chunk_size),
                                     content_type=c_type)
    response['Content-Length'] = os.path.getsize(
        os.path.join(settings.BASE_DIR, 'static', 'Files', 'output', 'pricelist.xlsx'))
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    return response
