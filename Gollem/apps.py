from django.apps import AppConfig


class GollemConfig(AppConfig):
    name = 'Gollem'
