import glob
import os
import pandas as pd
from pandas import DataFrame
from djongo import models

from GollemSite.Gollem_Intelligence import Constants


class Product(models.Model):
    sku = models.CharField(max_length=8)
    upc = models.BigIntegerField(primary_key=True)
    manufacturer = models.TextField()
    description = models.TextField()
    price = models.FloatField()
    inner_carton = models.IntegerField()
    outer_carton = models.IntegerField()
    small_image_url = models.TextField()
    large_image_url = models.TextField()
    stock = models.IntegerField()
    backorder = models.IntegerField()
    license = models.TextField()
    character = models.TextField()


class Stock:
    file_directory = None
    stock_df: DataFrame = pd.DataFrame()
    final_df: DataFrame = pd.DataFrame()

    df_headers = ['EID_CODE',
                  'EID_PERIGR',
                  'BARCODE',
                  'POSOT',
                  'TIM_POL1']

    def prepare_file(self):
        self.stock_df = self.stock_df[self.df_headers]
        self.stock_df.columns = ['sku', 'description', 'upc', 'stock', 'price']
        self.stock_df = self.stock_df[self.stock_df['sku'].notna()]

    def remove_special_products(self):
        """
            Drops all rows that do not meet our requirements(e.x description contains the word 'plush').

        """
        for code in Constants.NOT_NEEDED:
            self.final_df = pd.concat([self.final_df,
                                       self.stock_df[self.stock_df['sku'].str.startswith(code) &
                                                     ~self.stock_df['description'].str.contains(
                                                         Constants.NOT_NEEDED[code],
                                                         na=False)]],
                                      ignore_index=True)

    def amend_minus_stock(self):
        """
           The values at stock column cannot be less that 0.
           Therefore we need to fix those values, if any.
        """
        self.final_df.loc[self.final_df['stock'] < 0, 'stock'] = 0
        # TODO log all products that show minus stock and export the report in .xlsx


class Backorder:
    file_directory = None
    backorder_df = pd.DataFrame()
    final_df = pd.DataFrame()
    sku_column = 'Κωδικός Είδους'
    stock_column = 'Ποσότητα'

    def load_files(self, directory):
        self.file_directory = os.path.join(directory, Constants.BACKORDERS_DIR)
        os.chdir(self.file_directory)
        extension = 'xlsx'
        filenames = [i for i in glob.glob('*.{}'.format(extension))]

        for f in filenames:
            self.backorder_df = pd.read_excel(f, header=5)
            self.final_df = pd.concat(
                [self.final_df, self.backorder_df], sort=True)
            self.final_df = self.final_df[[self.sku_column, self.stock_column]]
            self.final_df = self.final_df[self.final_df[self.sku_column].notna(
            )]
            self.final_df.columns = ['sku', 'backorder']


class ExportedFile:
    merged_csv = pd.DataFrame()

    def load_files(self, directory):
        extension = 'csv'
        all_filenames = [i for i in glob.glob(os.path.join(
            directory, Constants.CSV_DIR, '*.{}'.format(extension)))]
        combined_csv = pd.concat([pd.read_csv(f, sep='\t')
                                  for f in all_filenames], sort=True)
        combined_csv.columns = ['License', 'URL',
                                'Description', 'SRP', 'Product Code']
        self.merged_csv = combined_csv


class Pricelist:
    final_df = pd.DataFrame()

    def __init__(self, directory):
        stock = Stock()
        backorder = Backorder()
        # TODO This should be removed. Data is stored in a DB now.
        stock.prepare_file(directory)
        backorder.load_files(directory)

        stock_df = stock.final_df
        backorder_df = backorder.final_df

        """ Merges the stock and backorder dataframe """
        self.final_df = stock_df.merge(backorder_df,
                                       on='sku',
                                       how='left')

        """ UPC column must be set as an object in order to display correctly the value """
        self.final_df = self.final_df.astype({'upc': 'object'})
