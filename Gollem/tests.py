import pandas
from django.test import TestCase

from Gollem.models import Stock


class StockFileTests(TestCase):
    def setUp(self):
        self.stock = Stock()

        self.stock.stock_df = pandas.DataFrame({'sku': ['UND00001', 'REN00002', 'UND00003', 'NEC00004', 'UND00005'],
                                                'description': ['Description for UND00001', 'Description for UND00002',
                                                                'Description for UND00003',
                                                                'Description for UND00004', 'Description for UND00005'],
                                                'upc': ['0000000000001', '0000000000002', '0000000000003',
                                                        '0000000000004',
                                                        '0000000000005'],
                                                'stock': [0, 15, 156, -154, 32],
                                                'price': ['12', '12', '12', '12', '12']})

        self.remove_special_products_expected_result = pandas.DataFrame(
            {'sku': ['UND00001', 'UND00003', 'NEC00004', 'UND00005'],
             'description': ['Description for UND00001', 'Description for UND00003', 'Description for UND00004',
                             'Description for UND00005'],
             'upc': ['0000000000001', '0000000000003', '0000000000004', '0000000000005'],
             'stock': [0, 156, -154, 32],
             'price': ['12', '12', '12', '12']}

            ).sort_values('sku').reset_index(drop=True)

        self.amend_minus_stock_expected_result = pandas.DataFrame(
            {'sku': ['UND00001', 'REN00002', 'UND00003', 'NEC00004', 'UND00005'],
             'description': ['Description for UND00001', 'Description for UND00002', 'Description for UND00003',
                             'Description for UND00004', 'Description for UND00005'],
             'upc': ['0000000000001', '0000000000002', '0000000000003', '0000000000004', '0000000000005'],
             'stock': [0, 15, 156, 0, 32],
             'price': ['12', '12', '12', '12', '12']}
        ).sort_values('sku').reset_index(drop=True)

    def test_remove_special_products(self):
        self.stock.remove_special_products()
        self.stock.final_df = self.stock.final_df.sort_values('sku').reset_index(drop=True)
        print(self.stock.final_df)
        print(self.remove_special_products_expected_result)

        # we need the reset_index(drop=True) method to
        self.assertTrue(pandas.DataFrame.equals(self.stock.final_df.reset_index(drop=True),
                                                self.remove_special_products_expected_result))

    def test_amend_minus_stock(self):
        self.stock.final_df = self.stock.stock_df
        self.stock.amend_minus_stock()
        self.stock.final_df = self.stock.final_df.sort_values('sku')

        print(self.stock.final_df)
        print(self.amend_minus_stock_expected_result)

        self.assertTrue(pandas.DataFrame.equals(self.stock.final_df.reset_index(drop=True),
                                                self.amend_minus_stock_expected_result))
