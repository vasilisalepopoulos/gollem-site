import fnmatch

from django.core.files.storage import FileSystemStorage
from dropbox import dropbox
from celery import Celery

from Gollem import models
from Gollem.models import *
from GollemSite.Gollem_Intelligence.Services import ThirdParty, api_global_variables

app = Celery('tasks', broker='redis://localhost:6379//')


# @app.task
# def generate_excel_pricelist(directory):
#
#     dropbox = ThirdParty.DropboxAPI()
#     file = models.File(directory)
#     raw_df = file.generate_pricelist()
#
#
#     missing_images_list = []
#
#     result = dropbox.connection.files_list_folder('/Images/small', recursive=True)
#     dropbox.process_entries(result.entries)
#
#     while result.has_more:
#         result = dropbox.connection.files_list_folder_continue(result.cursor)
#         dropbox.process_entries(result.entries)
#
#     logging.info(dropbox.list_images)
#     for supplier in Constants.WHOLESALER_CODES:
#         df = pd.read_excel(os.path.join(settings.BASE_DIR,
#                                         'static',
#                                         'Files',
#                                         Constants.OUTPUT_DIR,
#                                         'raw_pricelist.xlsx'), sheet_name=supplier)
#         for code in df['Product Code']:
#             print(code)
#             if any(code in item for item in dropbox.list_images):
#                 logging.info("Image ({}) already exist".format(code))
#             else:
#                 logging.info("Image ({}) doesn't".format(code))
#                 missing_images_list.append(code)
#     logging.info(missing_images_list)
#     for image in missing_images_list:
#         product_image = ProductImage(image)
#
#         gce = ThirdParty.GoogleCustomEngine(image)
#         url = gce.get_url_of_image()
#         product_image.define_type(url)
#         product_image.download_image(url)
#         try: # there are some issues with some pictures
#             dropbox.upload_new_small_image(os.path.join(settings.BASE_DIR,
#                                                         'static', 'Files', 'images', 'large', product_image.name+'.jpg'))
#             dropbox.upload_new_large_image(os.path.join(settings.BASE_DIR,
#                                                         'static', 'Files', 'images', 'large'))
#         except Exception:
#             logging.warning("error while uploading on product: "+ product_image.name)
#
#     priceList = File(directory)
#     priceList.dropbox = dropbox
#     priceList.insert_images()
#
#     file.generate_pricelist()


@app.task
def upload_backorder_files(request):
    backorder_files = request.FILES.getlist('backorder_files')
    fs = FileSystemStorage(location="static/Files/backorders")

    for file in os.listdir("static/Files/backorders"):
        fs.delete(os.path.join("static/Files/backorders", file))
        os.remove(os.path.join("static/Files/backorders", file))

    for file in backorder_files:
        fs.save(file.name, file)


@app.task
def upload_stock_files(request):
    stock_file = request.FILES['stock_files']
    fs = FileSystemStorage(location="static/Files/stock")

    for file in os.listdir("static/Files/stock"):
        fs.delete(os.path.join("static/Files/stock", file))
        os.remove(os.path.join("static/Files/stock", file))

    fs.save(stock_file.name, stock_file)


@app.task
def upload_exported_files(request):
    csv_files = request.FILES.getlist('exported_files')
    fs = FileSystemStorage(location="static/Files/csv files")

    for file in os.listdir("static/Files/csv files"):
        fs.delete(os.path.join("static/Files/csv files", file))
        os.remove(os.path.join("static/Files/csv files", file))

    for file in csv_files:
        fs.save(file.name, file)
