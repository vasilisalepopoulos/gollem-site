from __future__ import absolute_import, unicode_literals
from celery import Celery
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'GollemSite.settings')

app = Celery('scheduler',
             broker='redis://localhost:6379',
             include=['scheduler.tasks'])

if __name__ == '__main__':
    app.start()
